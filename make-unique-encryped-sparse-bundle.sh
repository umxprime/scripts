#!/bin/sh

BUNDLE_SIZE_GB=20
SPARSE_BAND_SIZE=307200 # 150MB expressed in 512 bytes sectors
FILE_NAME=`uuidgen`
VOLUME_NAME=$1
OUTPUT_PATH=$2
BUNDLE_OUTPUT_PATH="${OUTPUT_PATH}/${FILE_NAME}.sparsebundle"
echo "Will create ${BUNDLE_OUTPUT_PATH} ..."

hdiutil create \
-fs APFS \
-size ${BUNDLE_SIZE_GB}g \
-type SPARSEBUNDLE \
-encryption AES-128 \
-imagekey sparse-band-size=${SPARSE_BAND_SIZE} \
-volname ${VOLUME_NAME} \
${OUTPUT_PATH}/${FILE_NAME}.sparsebundle