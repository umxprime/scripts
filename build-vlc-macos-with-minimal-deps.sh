set -exo pipefail

SCRIPT_FOLDER_PATH=`cd $(dirname $0) && pwd`
VLC_ROOT_PATH=$SCRIPT_FOLDER_PATH/../vlc
BUILDS_PATH=$SCRIPT_FOLDER_PATH/../vlc-builds
MINIMAL_OSX_VERSION="10.11"
export SDKROOT=$(xcrun --show-sdk-path)

REBUILD_TOOLS="yes"
REBUILD_CONTRIBS="yes"
FORCE_VLC_BOOTSTRAP="yes"
FORCE_VLC_RECONFIGURE="yes"
REBUILD_LIBVLC="yes"

CORE_COUNT=`getconf NPROCESSORS_ONLN 2>&1`
let JOBS=$CORE_COUNT+1

export AR="$(xcrun --find ar)"
export CC="$(xcrun --find clang)"
export CXX="$(xcrun --find clang++)"
export NM="$(xcrun --find nm)"
export OBJC="$(xcrun --find clang)"
export OBJCXX="$(xcrun --find clang++)"
export RANLIB="$(xcrun --find ranlib)"
export STRIP="$(xcrun --find strip)"

HOST_ARCH="aarch64"
COMPILER_HOST_ARCH=$HOST_ARCH
if [ "$COMPILER_HOST_ARCH" = "aarch64" ]; then
    COMPILER_HOST_ARCH="arm64"
fi
BUILD_ARCH=`uname -m | cut -d. -f1`
if [ "$BUILD_ARCH" = "arm64" ]; then
    BUILD_ARCH="aarch64"
fi
OSX_KERNELVERSION=$(uname -r | cut -d. -f1)

BUILD_TRIPLET="$BUILD_ARCH-apple-darwin$OSX_KERNELVERSION"
HOST_TRIPLET="$HOST_ARCH-apple-darwin$OSX_KERNELVERSION"

BUILD_PATH="$BUILDS_PATH/macos-$HOST_TRIPLET"

PYTHON_PATH=$(echo /Library/Frameworks/Python.framework/Versions/3.*/bin | awk '{print $1;}')
if [ ! -d "$PYTHON_PATH" ]; then
    PYTHON_PATH=""
fi
export PATH="${VLC_ROOT_PATH}/extras/tools/build/bin:${VLC_ROOT_PATH}/contrib/${BUILD_TRIPLET}/bin:$PYTHON_PATH:${VLC_PATH}:/bin:/sbin:/usr/bin:/usr/sbin"

#
# vlc/extras/tools
#

pushd "$VLC_ROOT_PATH/extras/tools"
./bootstrap
if [ "$REBUILD_TOOLS" = "yes" ]; then
    make clean
    ./bootstrap
fi
make -j1
popd

#
# vlc/contribs
#

export CFLAGS="-Werror=partial-availability"
export CXXFLAGS="-Werror=partial-availability"
export OBJCFLAGS="-Werror=partial-availability"
export EXTRA_CFLAGS="-isysroot $SDKROOT -mmacosx-version-min=$MINIMAL_OSX_VERSION -DMACOSX_DEPLOYMENT_TARGET=$MINIMAL_OSX_VERSION -arch $COMPILER_HOST_ARCH"
export EXTRA_LDFLAGS="-isysroot $SDKROOT -mmacosx-version-min=$MINIMAL_OSX_VERSION -DMACOSX_DEPLOYMENT_TARGET=$MINIMAL_OSX_VERSION -arch $COMPILER_HOST_ARCH"
export XCODE_FLAGS="MACOSX_DEPLOYMENT_TARGET=$MINIMAL_OSX_VERSION -sdk $SDKROOT WARNING_CFLAGS=-Werror=partial-availability"

pushd "$VLC_ROOT_PATH/contrib"
if [ "$REBUILD_CONTRIBS" = "yes" ]; then
    rm -rf "contrib-$HOST_TRIPLET"
    rm -rf "$HOST_TRIPLET"
fi

mkdir -p "contrib-$HOST_TRIPLET" && cd "contrib-$HOST_TRIPLET"
../bootstrap \
--build=$BUILD_TRIPLET \
--host=$HOST_TRIPLET \
--disable-optim \
--disable-all \
--enable-a52 \
--enable-ffmpeg \
--enable-dvbpsi \
--enable-matroska

make list
make fetch
# make -j$JOBS .gettext
make -j$JOBS -k || make -j1
popd

unset CFLAGS
unset CXXFLAGS
unset OBJCFLAGS
unset EXTRA_CFLAGS
unset EXTRA_LDFLAGS
unset XCODE_FLAGS

#
# vlc/bootstrap
#

export CFLAGS="-g -O0 -arch $COMPILER_HOST_ARCH"
export CXXFLAGS="-g -O0 -arch $COMPILER_HOST_ARCH"
export OBJCFLAGS="-g -O0 -arch $COMPILER_HOST_ARCH"
export LDFLAGS="-arch $COMPILER_HOST_ARCH"

pushd "${VLC_ROOT_PATH}"
if [ "$FORCE_VLC_BOOTSTRAP" = "yes" ]; then
    rm -f "${VLC_ROOT_PATH}/configure"
fi
if ! [ -e "${VLC_ROOT_PATH}/configure" ]; then
    ${VLC_ROOT_PATH}/bootstrap
fi
popd

#
# vlc/configure
#

mkdir -p "$BUILD_PATH"
pushd "$BUILD_PATH"

if [ "$FORCE_VLC_RECONFIGURE" = "yes" ]; then
    rm -f Makefile
fi

if [ "${VLC_ROOT_PATH}/configure" -nt Makefile ]; then
    sh ${VLC_ROOT_PATH}/configure \
    --build=$BUILD_TRIPLET \
    --host=$HOST_TRIPLET \
    --with-macosx-version-min=$MINIMAL_OSX_VERSION \
    --with-macosx-sdk=$SDKROOT \
    --prefix="$BUILD_PATH/install" \
    --disable-sparkle \
    --enable-macosx \
    --enable-macosx-avfoundation \
    --disable-optimizations \
    --enable-lua=no \
    --enable-merge-ffmpeg
fi

#
# make libvlc
#

if [ "$REBUILD_LIBVLC" = "yes" ]; then
    make clean
fi

make -j$JOBS

make VLC.app

unset CFLAGS
unset CXXFLAGS
unset OBJCFLAGS
unset LDFLAGS
popd