#!/bin/sh

SOURCE_FILE_PATH="${1}"
OUTPUT_FOLDER_PATH="${2}"
OUTPUT_FILENAME="${3}"

INTERMEDIATE_FILEDIR="${OUTPUT_FOLDER_PATH}"
INTERMEDIATE_FILENAME="${OUTPUT_FILENAME}"
INTERMEDIATE_FILEPATH="${INTERMEDIATE_FILEDIR}/${INTERMEDIATE_FILENAME}.avi"
OUTPUT_FILEPATH="${OUTPUT_FOLDER_PATH}/${OUTPUT_FILENAME}.amv"

ffmpeg -i "${SOURCE_FILE_PATH}" \
-f avi \
-ac 1 \
-ar 22050 \
-c:v copy \
-c:a pcm_s32le \
"${INTERMEDIATE_FILEPATH}"

ffmpeg -i "${INTERMEDIATE_FILEPATH}" \
-f amv \
-s 320x240 \
-vf fps=25 \
-r 25 \
-vstrict -1 \
-block_size 882 \
"${OUTPUT_FILEPATH}"

rm "${INTERMEDIATE_FILEPATH}"